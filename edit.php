<?php session_start();
require_once __DIR__ . "/config/config.php";
require_once __DIR__ . "/lib/database.php";
require_once  __DIR__ . "/models/auto.php";

if (isset($_SESSION["name"])) {
    $username = $_SESSION["name"];
} else {
    die("ACCESS DENIED");
    //header("Location: index.php");
}
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    if (isset($_POST['submit'])) {
        $auto_update = new Auto($_POST['brand'], $_POST['v-year'], $_POST['kms']);
        $auto_update->makeConnection();
        $auto_update->updateAuto($id);
        header("Location: autos.php") and die();
    }
} else {
    header("Location: /a21/index.php") and die();
}




require __DIR__ . "/views/edit.views.php";
