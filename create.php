<?php session_start();
require_once __DIR__ . "/config/config.php";
require_once __DIR__ . "/lib/database.php";
require_once  __DIR__ . "/models/auto.php";
if (isset($_SESSION["name"])) {
    $autos = new Auto();
    $autos->makeConnection();
    $username = $_SESSION["name"];
} else {
    die("ACCESS DENIED");
    //header("Location: index.php");
}
if ((isset($_POST['submit']) && (is_numeric($_POST['v-year'])) && (is_numeric($_POST['kms'])) && (!empty($_POST['brand'])))) {
    $auto_i = new Auto($_POST['brand'], $_POST['v-year'], $_POST['kms']);
    $auto_i->makeConnection();
    $auto_i->addAuto();
    $_SESSION['success'] = "Registro insertado";
    header("Location: autos.php");
    return;
?>
    <div class="card">
        <div class="card-body text-success text-center">
            <?php if (isset($_SESSION['success'])) {
                echo $_SESSION['success'];
                unset($_SESSION['success']);
            } ?>
        </div>
    </div>

<?php
} elseif ((isset($_POST['submit']) && !(is_numeric($_POST['v-year'])) && !(is_numeric($_POST['kms']) && (!empty($_POST['brand']))))) { ?>
    <div class="card">
        <div class="card-body text-danger text-center">
            Kilometraje y año deben ser numéricos.
        </div>
    </div>
<?php
} elseif (empty($_POST['brand']) && isset($_POST['submit'])) { ?>
    <div class="card">
        <div class="card-body text-danger text-center">
            El campo marca no puede estar vacío.
        </div>
    </div>
<?php
}

require_once __DIR__ . "/views/create.view.php";
