<?php session_start();
require_once __DIR__ . "/config/config.php";
require_once __DIR__ . "/lib/database.php";
require_once  __DIR__ . "/models/auto.php";
if (isset($_SESSION["name"])) {
    $autos = new Auto();
    $autos->makeConnection();
    $username = $_SESSION["name"];
} else {
    die("ACCESS DENIED");
    //header("Location: index.php");
}
if ((isset($_POST['crt']))) {
    header('location: create.php') and die();
}
if ((isset($_POST['id']) && (!empty($_POST['id']))) && (isset($_POST['dlt']))) {
    header('location: delete.php?id=' . urlencode($_POST['id'])) and die();
}
if (isset($_POST['logout'])) {
    header('Location: logout.php') and die();
}
if ((isset($_POST['id']) && (!empty($_POST['id']))) && (isset($_POST['edit']))) {
    header('location: edit.php?id=' . urlencode($_POST['id'])) and die();
}
require __DIR__ . "/views/autos.views.php";
