<?php session_start();
if (isset($_POST["email"]) && isset($_POST['password'])) {

    $salt = 'XyZzy12*_';

    $md5 = hash('md5', 'XyZzy12*_php123');

    if ((hash('md5', $salt . $_POST["password"]) === $md5) && (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))) {
        error_log("Login success" . $_POST['email']);
        $_SESSION['name'] = $_POST['email'];
        header("Location: autos.php") and die();
    } else if ((empty($_POST["email"]) || empty($_POST["password"]))) {
        $_SESSION['error'] = "Se requiere email y clave para acceder";
        header("Location: login.php") and die();
    } else {
        if (!(hash('md5', $salt . $_POST["password"]) === $md5)) {
            $_SESSION['error'] = "Contraseña incorrecta";
            error_log("Login fail" . $_POST['email'] . hash('md5', $salt . $_POST["password"]));
            header("Location: login.php") and die();
        }
        if (!(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))) {
            $_SESSION['error'] = "Formato de email incorrecto";
            error_log("Login fail" . $_POST['email'] . hash('md5', $salt . $_POST["password"]));
            header("Location: login.php") and die();
        }
    }
}
require_once __DIR__ . "/views/login.views.php";
