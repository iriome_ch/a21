<?php
class Database
{
    private $host = DB_HOST;

    private $user = DB_USER;

    private $pass = DB_PASS;

    private $dbname = DB_NAME;


    private $dbh;

    private $stmt;

    private $error;

    public function __construct()

    {
        $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;
        $options = [
            //Comando que se ejecuta al conectar a MySQL.
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            //Permite hacer una conexion persistente a la base de datos.
            PDO::ATTR_PERSISTENT => true,
            //Activa el reporte de errores en el modo que lanza excepciones.
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ];
        try {
            $this->dbh = new PDO($dsn, $this->user, $this->pass, $options);
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            echo $this->error;
        }
    }

    public function query(string $SQL)
    {
        $this->stmt = $this->dbh->prepare($SQL);
    }

    public function bind($param, $value, $type = null)
    {
        if (is_null($type)) {

            switch (true) {
                    //comprueba que la variable pasada por parametros es un numero entero y si es asi hace que el type que se pasara posteriormente al bindValue() sea tipo entero.
                case is_int($value):

                    $type = PDO::PARAM_INT;

                    break;
                    //comprueba que la variable pasada por parametros es un boleano y si es asi hace que el type que se pasara posteriormente al bindValue() sea tipo boleano.
                case is_bool($value):

                    $type = PDO::PARAM_BOOL;

                    break;
                    //comprueba que la variable pasada por parametros es un null y si es asi hace que el type que se pasara posteriormente al bindValue() sea null.
                case is_null($value):

                    $type = PDO::PARAM_NULL;

                    break;
                    //hace que el type que se pasara posteriormente al bindValue() sea string.
                default:

                    $type = PDO::PARAM_STR;
            }
        }
        //Vincula el valor que se quiere usar con la sentencia mysql y establece mediante el type el tipo de valor que se va a introducir.
        $this->stmt->bindValue($param, $value, $type);
    }

    public function execute()
    {
        return $this->stmt->execute();
    }
    //Ejecuta la sentencia mysql preparada anteriormente y devuelve un objeto anonimo con los datos y atributos de cada columna de la tabla resultante de la sentencia.
    public function resultSet()
    {

        $this->execute();

        return $this->stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public function single()
    {

        $this->execute();

        return $this->stmt->fetch(PDO::FETCH_OBJ);
    }
};
