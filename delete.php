<?php session_start();
require_once __DIR__ . "/config/config.php";
require_once __DIR__ . "/lib/database.php";
require_once  __DIR__ . "/models/auto.php";

if (isset($_SESSION["name"])) {
    $username = $_SESSION["name"];
} else {
    die("ACCESS DENIED");
    //header("Location: index.php");
}
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    if (isset($_POST['submit']) && $_POST['submit'] == "true") {
        $auto_d = new Auto();
        $auto_d->makeConnection();
        $auto_d->delAuto($id);
        header("Location: autos.php") and die();
    } else if (isset($_POST['submit']) && $_POST['submit'] == "false") {
        header("Location: autos.php") and die();
    }
} else {
    header("Location: /a21/index.php") and die();
}




require __DIR__ . "/views/delete.views.php";
