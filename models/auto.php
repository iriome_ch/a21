<?php
class Auto
{
    private $db;



    private $make;

    private $year;

    private $mileage;



    public function __construct($make = '', $year = 0, $mileage = 0)
    {
        $this->make = $make;

        $this->year = $year;

        $this->mileage = $mileage;
    }

    public function makeConnection()
    {
        $this->db = new Database();
    }

    public function getAutos()
    {

        $this->db->query("SELECT auto_id, make, year,mileage FROM autos");

        $results = $this->db->resultSet();

        return $results;
    }

    public function addAuto()

    {

        $this->db->query('insert into autos (make,year,mileage) Values (:mk,:yr,:mi)');

        $this->db->bind(':mk', $this->getMake());

        $this->db->bind(':yr', $this->getYear());

        $this->db->bind(':mi', $this->getMileage());

        $this->db->execute();
    }

    /**
     * Get the value of make
     */
    public function getMake()
    {
        return $this->make;
    }

    /**
     * Set the value of make
     */
    public function setMake($make): self
    {
        $this->make = $make;

        return $this;
    }

    /**
     * Get the value of year
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set the value of year
     */
    public function setYear($year): self
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get the value of mileage
     */
    public function getMileage()
    {
        return $this->mileage;
    }

    /**
     * Set the value of mileage
     */
    public function setMileage($mileage): self
    {
        $this->mileage = $mileage;

        return $this;
    }

    public function delAuto($id)
    {
        $this->db->query("SELECT * FROM autos where auto_id=$id");
        $results = $this->db->resultSet();
        if (!$results == null) {
            $auto_d = $results[0];

            $this->db->query("DELETE from autos where auto_id = $auto_d->auto_id");
            $this->db->execute();
        }
    }

    public function rowCount()
    {
        $this->db->query("SELECT * from autos");
        $results = $this->db->resultSet();
        if (count($results) == 0) {
            return 0;
        } else {
            return count($results);
        }
    }

    //Update
    public function updateAuto($id)
    {

        $this->db->query("UPDATE autos SET make = :mk, year = :yr, mileage = :mi WHERE auto_id = :id");
        $this->db->bind(':mk', $this->getMake());
        $this->db->bind(':yr', $this->getYear());
        $this->db->bind(':mi', $this->getMileage());
        $this->db->bind(':id', $id);
        $this->db->execute();
    }

    public function getAutoById($id)
    {
        $this->db->query("SELECT * FROM autos where auto_id=$id");
        $results = $this->db->single();
        if (!$results == null) {
            $auto_d = $results;

            $this->setMake($auto_d->make);
            $this->setYear($auto_d->year);
            $this->setMileage($auto_d->mileage);
        }
    }
}
