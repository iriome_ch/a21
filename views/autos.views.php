<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>

<body>
    <div class="container">
        <div class="row align-items-center">
            <div class="col align-items-center">
                <p><img src="https://img.icons8.com/ios/50/000000/user--v3.png" /> <?php echo $username; ?></p>
            </div>
            <div class="col align-items-center">
                <form action="" method="post">
                    <button type="submit" class="btn btn-outline-success" name="crt">Create</button>
                    <button type="submit" class="btn btn-outline-dark" name="logout">log out</button>
                </form>
            </div>
        </div>
        <div class="row align-items-center ">
            <div class="col p-6">
                <?php
                $rows = $autos->getAutos();
                if (count($rows) > 0) {
                ?>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Brand</th>
                                <th>Kilometres</th>
                                <th>Years</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($rows as $row) {

                            ?>
                                <tr>
                                    <th><?= $row->auto_id; ?></th>
                                    <td><?= $row->make; ?></td>
                                    <td><?= $row->mileage; ?></td>
                                    <td><?= $row->year; ?></td>
                                    <td>
                                        <form action="" method="post">
                                            <input type="hidden" name="id" value="<?= $row->auto_id; ?>">
                                            <input type="submit" class="btn btn-outline-danger" name="dlt" value="Delete">
                                            <input type="submit" class="btn btn-outline-danger" name="edit" value="Edit">
                                        </form>
                                    </td>
                                </tr>
                            <?php
                            }
                        } else {
                            ?>
                            <div class="card">
                                <div class="card-body text-center">
                                    No results found
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>

</html>